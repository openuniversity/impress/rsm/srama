
### Check whether required packages are installed; if not,
### install them. Note that we don't load the packages
### with 'require' or 'library' to make sure we never
### forget to use the double colon operator to indicate
### the package from which we want to use each function.
### 
### Note that what we do here is pretty sophisticated/complex:
### we use 'lapply' to sequentially process each element of a
### vector (combined from single values using the 'c' function),
### and in the anonymous function we create to do the actual
### processing (where the value currently being processed is
### internally referred to as 'pkg'), we check whether the
### package is already installed by looking in the row names
### of the dataframe returned by the 'installed.packages'
### function; and if it isn't installed, we install it using
### 'install.packages'. Finally, we use 'invisible' to prevent
### the result of the 'lapply' function from printing to the
### console.
invisible(lapply(c('here',
                   'readxl',
                   'dplyr',
                   'userfriendlyscience',
                   'metafor'),
                  function(pkg) {
                    if (!(pkg %in% row.names(installed.packages())))
                      install.packages(pkg);
                  }));

### Use the 'read_excel' fuction in the 'readxl' package
### to load the three worksheets from the spreadsheet
studyDat <-
  readxl::read_excel(here::here('data-precleaning',
                                'MA-coded-data.xlsx'),
                     sheet=1);
rDat <-
  readxl::read_excel(here::here('data-precleaning',
                                'MA-coded-data.xlsx'),
                     sheet=2);
### We won't use this, just importing for completeness' sake
dDat <-
  readxl::read_excel(here::here('data-precleaning',
                                'MA-coded-data.xlsx'),
                     sheet=3);

### Compile list of ids to retain. The constraints are:
###
### - Only studies that provided both Pearson's r and N
### - Only studies that measured behavior
### - Only observational studies
### - Only studies with degree of identification
idsToRetain <-
  studyDat$Code[(rowSums(studyDat[, c('Intention yes/no',
                                      'Intention cont',
                                      'Atitude yes/no',
                                      'Attitude cont')]) == 0) &
                (studyDat$`Type of study` == "survey") &
                (studyDat$`Degree of identification` == 1)];

### Remove variables that can be removed
studyDat <-
  studyDat[, c('Code',
               'Authors',
               'Year',
               'Type of respondents',
               'Country',
               'Paper vs thesis',
               'Behavior related SI',
#               'Degree of identification',
               'HB type general categories',
               'Pos or Neg health behavior',
               # 'Behavior yes/no',
               # 'Behavior cont',
               # 'Intention yes/no',
               # 'Intention cont',
               # 'Atitude yes/no',
               # 'Attitude cont',
               NULL)];

### Rename to more useable names
names(studyDat) <-
  c('id',
    'authors',
    'year',
    'population',
    'country',
    'pubType',
    'behaviorRelatedSI',
    'behaviorCategory',
    'behaviorValence')

### Clean dataframe with correlations
rDat <-
  rDat[, c('Code',
           'correlation',
           'total N')];

names(rDat) <-
  c('id',
    'r',
    'n');

### Remove studies we want to exclude from both dataframes
studyDat <-
  studyDat[studyDat$id %in% idsToRetain, ];
rDat <-
  rDat[rDat$id %in% idsToRetain, ];

### Merge the dataframes into one dataframe; the dataframe
### with the effect sizes is the primary dataframe, because
### we want to append the study details to every effect size.
### We simply overwrite the effect size dataframes with the
### merged versions.
dat <- merge(x = rDat,
             y = studyDat,
             by = 'id');
# dDat <- merge(x = dDat,
#               y = studyDat,
#               by = 'Code');

### Use the 'bind_rows' function in the 'dplyr' package
### to combine the two extended effect size dataframes
# dat <-
#   dplyr::bind_rows(rDat,
#                    dDat);

### Store study identifier separately
dat$studyId <-
  gsub("^([a-zA-Z0-9]+)\\..*",
       "\\1",
       dat$id);

### Aggregate correlations within each study to a single
### effect size measure

### First compute effect variances for the correlation measures
### (will be deleted before saving)
dat[, c('r_yi', 'r_vi')] <-
  metafor::escalc(ri = dat$r,
                  ni = dat$n,
                  measure = "COR");

fullDat <- dat;
dat <-
  do.call(rbind,
          by(fullDat,
             INDICES = list(fullDat$studyId, fullDat$behaviorCategory),
             FUN = function(dat) {
               if (nrow(dat) == 0) {
                 return(NULL);
               } else if (nrow(dat) == 1) {
                 return(dat);
               } else {
                 maRes <- metafor::rma(yi = dat$r_yi,
                                       vi = dat$r_vi);
                 res <- dat[1, ];
                 res$r <- maRes$b;
                 return(res);
               }
           }));

### Store resulting dataframe for import in example
write.csv(x = dat[, !(names(dat) %in% c('r_yi', 'r_vi'))],
          file = here::here("srama-example-data.csv"),
          row.names = FALSE);
