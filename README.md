# Systematic Reviews And Meta-Analyses

This is the repository for the IMPReSS RSM training "Systematic Reviews And Meta-Analyses" (SRAMA). The rendered version of the main R Markdown file for this repository is hosted by GitLab Pages at https://openuniversity.gitlab.io/impress/rsm/srama. The Open Science Framework repository for this training is located at https://osf.io/7wvf5 (and for the sake of completeness, the URL for this GitLab repository is https://gitlab.com/openuniversity/impress/rsm/srama).
